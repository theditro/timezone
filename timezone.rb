require 'excon'
require 'pry'
require 'json'

#	list of time zones:
#	https://timezonedb.com/time-zones

class TimeZone
	
	def initialize(time_zone_name, key = "1TGT5R7A8OLU",url="http://api.timezonedb.com/v2.1/get-time-zone?key=1TGT5R7A8OLU&format=json&by=zone&zone=")
	@time_zone_name = time_zone_name
	@key = key
	@url = url
	end
	
	
	def response
	 Excon.get("#{@url}#{@time_zone_name}").body
	end
	 def body
    JSON.parse(response)
  end
	
end

Tm = TimeZone.new("Europe/Kiev")	
Tm.response
print Tm.body
Tm.pp